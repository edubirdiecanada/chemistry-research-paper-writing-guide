# Chemistry Research Paper Writing Guide

Are scheduled to write a chemistry [research paper](https://www.artsci.uc.edu/content/dam/refresh/artsandsciences-62/departments/chemistry/docs/UndergraduateResearchTopics2018.pdf) but aren’t sure where to begin?
You have landed on the right page.

In this quick chemistry research paper writing guide, we’re going to teach you the basics of how to write an excellent chemistry research paper. 
As a rule, scientific research entails the ability to demonstrate original and ground-breaking ideas to your targeted audience.
The purpose of this paper is not to demonstrate what has already been researched or the scientific process involved in the study.
To write a winning research paper in chemistry, you need to narrate, articulate internal prose that resulted in the final findings.
It makes sense to seek [college chemistry assignment help online](https://ca.edubirdie.com/chemistry-help) if you aren’t sure how to go about the entire writing process. When you seek chemistry help, you relieve yourself of the hard work and headaches associated with this kind of complex paper.
That said, let’s get into the basic foundation of writing a winning chemistry research paper.

### An Excellent Research Paper Starts With a Hook
To write an excellent research paper in chemistry, you need to start with a hook. This starts with your topic. First off, your topic needs to be attention-grabbing.
For instance, you can write a research paper in chemistry about a disease that has affected millions of people. To make your research paper stand out you have must give it an engaging title.

### Getting the Introduction Right
Once you have hooked the reader, your introduction needs to keep them reading. It’s important to note that the introduction is different from a short abstract. 
Your introductory paragraph should provide the background of the paper and demonstrate why it’s important in the context. In other words, it should demonstrate the critical questions your project intends to address and why the reader should be interested.
This is where you create a strong thesis statement as well. 
To write an attention-grabbing intro, follow the tips below:
1. Start with problem description
1. Base your paper on a true story
1. Interpret the issue you intend to discuss vividly
1. Give flashback

Open with a joke, fact, statistic or you can even quote a renowned author.
Remember to keep everything formal. Avoid using jargon or slang in your language. 

### Crafting the Body Section
Once you have nailed the introduction, your last sentence should lead the reader to the body section.
In this section, you need to explain, prove your point while supporting your thesis statement with solid evidence. Remember to include your evidence seamlessly in a way that rhymes with the entire paper.
In other words, your evidence should be closely related to the entire research paper so that the reader can connect facts easily.
If you use quotes, provide the sources. When quoting, you can use terms like ‘comments’, ‘confirms’, ‘explains, etc.
Demonstrate why the evidence provided is crucial to your argument. Remember that no matter how solid your argument is, it won’t make sense if your audience cannot connect the dots. Your evidence must prove a point.

### Wrapping Up Your Research Paper
The conclusion paragraph is like the final chord in a sweet song. It needs to make the reader feel that the paper is organized and completed successfully.
You want anyone reading the paper to feel that you backed up each claim you have made in your thesis.
Restate the thesis statement and tip up the key points while keeping the conclusion succinct and clear.
Once done with this, [cite](https://libguides.pasadena.edu/c.php?g=126825&p=829714) your sources accordingly.
